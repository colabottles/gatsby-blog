---
title: "Day Two, 100 Days Of Gatsby"
date: "2020-01-02"
---
Worked more on the blog so far to get the posting working and getting used to using GraphQL. Which, for the most part, is pretty cool to work with I will admit that. Me, being an old 'purist' with CSS and HTML, using a CMS or some sort of 'anti-JS' mindset, have become more open to JavaScript and learning a framework such as Vue and Ember.

I also wanted to dive into Gatsby after hearing Marcy Sutton talk at An Event Apart Boston in 2019 and how she was pitching accessibility in JavaScript and had a great talk that she gave. Also having been super helpful and friendly when it came to guiding me along when I had some questions. Grateful to have heard her talk.

As of this post, I am eagerly awaiting the Day Two email and jump on that!