---
title: "Day Five, 100 Days Of Gatsby"
date: "2020-01-05"
---

Spent most of the morning tinkering around with an error with eslint and the SEO plugin so I just removed the `import` for now and it seems like there are no errors.

I figure that would be enough for the day and on Monday, I will tend to straightening out the 404 page issue that's going on as well as get some more links up to the appropriate pages.

Fixed the contact page and put a form in, still working on the SEO issue because Netlify wouldn't deploy it on the fix I got from Twitter earlier, and did some styling changes and About page change.

EDIT: Fixed the SEO plugin deploymentissue, it was due to an error with capitalization. So now it is deployed and all set to go and just publish away!