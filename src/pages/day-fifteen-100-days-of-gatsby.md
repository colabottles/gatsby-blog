---
title: "Day Fifteen, 100 Days Of Gatsby"
date: "2020-01-15"
---

Challenge #3 from the folks at GatsbyJS (Shoutout to [@hashim_warren](https://twitter.com/hashim_warren) ) was;

<blockquote>
<strong>Challenge 3: Learn How to Use Images with Gatsby</strong>

Gatsby sites are known for their blazing fast speed. The way a Gatsby site optimizes images is a big reason for that.

This week’s challenge invites you to add images to your blog and learn about a popular and powerful Gatsby feature, <strong>gatsby-image</strong>!
</blockquote>

Still dealing with the aftermath of the surgery yesterday, I'll be doing this when I feel better. Not 100% yet, but almost there. Until then, that is all.

EDIT: [Check out the About page](https://todds-gatsby-blog.netlify.com/about/). I think I nailed it. 🤷🏻‍♂️