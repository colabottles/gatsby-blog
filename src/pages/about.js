import React from "react"
import { useStaticQuery, graphql } from "gatsby"
import Img from 'gatsby-image'
import Layout from "../components/layout"

export default () => {
  const data = useStaticQuery(graphql`
    query {
      file(relativePath: { eq: "images/todd.png" }) {
        childImageSharp {
          fluid(maxWidth: 400, quality: 100) {
            ...GatsbyImageSharpFluid
          }
        }
      }
    }
  `)

  return (
    <Layout>
      <h1>About Me</h1>
      <Img
        fluid={data.file.childImageSharp.fluid}
        alt="Todd Libby"
      />
      <p>Here's just a snippet about me. I've been a designer and developer for twenty years. I got interested in programming and computers at nine years old. I'm currently just futzing around with Gatsby and JavaScript. Yes, even React. More to come, that is all.</p>
    </Layout>
  )
}



