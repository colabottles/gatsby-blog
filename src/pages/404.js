import React from "react"
import Layout from "../components/layout"

export default () => (
  <Layout>
    <h1>Page Not Found</h1>
    <p>Oops, We couldn't find this page! Please try again!</p>
  </Layout>
)