---
title: "Day Nine, 100 Days Of Gatsby"
date: "2020-01-09"
---

Today was not a really great day and I have decided to wrap up everything and walk away from the web. It's not an easy decision, but [I posted about it on my blog] (https://toddl.dev/posts/goodbye/).