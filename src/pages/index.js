import React from "react"
import { css } from "@emotion/core"
import { Link, graphql } from "gatsby"
import { rhythm } from "../utils/typography"
import Helmet from "react-helmet"
import Container from "../components/container"
import styles from "./about-css-modules.module.css"
import Layout from "../components/layout"

const User = props => (
  <div className={styles.user}>
    <img src={props.avatar} className={styles.avatar} alt="" />
    <div className={styles.description}>
      <h2 className={styles.username}>{props.username}</h2>
      <p className={styles.excerpt}>{props.excerpt}</p>
    </div>
  </div>
)

export default ({ data }) => {
  return (
    <Layout>
      <div class="wrapper">
        <Helmet>
          <title>My Gatsby Blog</title>
        </Helmet>
        <Container>
          <User
            username="Todd Libby"
            avatar="https://toddl.dev/img/headshot.png"
            excerpt="I'm Todd Libby. Designer, developer, lobster roll connaisseur."
          />
        </Container>
        <h2>{data.allMarkdownRemark.totalCount} Posts</h2>
        {data.allMarkdownRemark.edges.map(({ node }) => (
          <div key={node.id}>
            <Link
              to={node.fields.slug}
              css={css`
                text-decoration: none;
                color: inherit;
              `}
            >
              <h3
                css={css`
                  margin-bottom: ${rhythm(1 / 4)};
                  border-bottom: 1px solid #000;
                  padding: 1rem 0;
                `}
              >
                {node.frontmatter.title}{" "}
                <span
                  css={css`
                    color: #f2f76f;
                  `}
                >
                  — {node.frontmatter.date}
                </span>
              </h3>
              <p>{node.excerpt}</p>
            </Link>
          </div>
        ))}
      </div>
    </Layout>
  )
}

export const query = graphql`
  query {
    allMarkdownRemark(sort: { fields: [frontmatter___date], order: DESC }) {
      totalCount
      edges {
        node {
          id
          frontmatter {
            title
            date(formatString: "DD MMMM, YYYY")
          }
          fields {
            slug
            readingTime {
              text
            }
          }
          excerpt
        }
      }
    }
  }
`