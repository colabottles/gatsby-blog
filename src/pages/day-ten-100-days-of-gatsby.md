---
title: "Day Ten, 100 Days Of Gatsby"
date: "2020-01-10"
---

I do not like to leave things unfinished. I also stay true to my word. Which is why this decision to walk away from something I love so dearly (the web, programming), is the hardest decision I have ever had to make.

I'm going to do this #100DayOfGatsby and finish it. I started it, I'll see it through. I'll be taking the weekend to decide what comes next. I'm not going to bow out and quit.