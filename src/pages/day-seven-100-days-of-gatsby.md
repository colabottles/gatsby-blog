---
title: "Day Seven, 100 Days Of Gatsby"
date: "2020-01-07"
---

After a day off, I started work on making accessibility a priority, like I do on everything web related I work on. I always make time and include it into the process whenever I do any work on the web.

Used `react-helmet` I found out to add the `<title>Title tag</title>` for the site when WAVE couldn't find it originally. There was more I wanted to do, but I got caught in a rabbit hole of looking into using [sanity.io](https://sanity.io) and some other stuff I had been working on during the day.

I did manage to fix the contrast errors on the timestamp as well, so I did get a little bit of `#a11y` done. On to Day 8!