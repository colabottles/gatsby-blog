---
title: "Day Three, 100 Days Of Gatsby"
date: "2020-01-03"
---

Day Three was spent trying to get the chronological order fixed with this blog to no avail. I didn't work on it long before I went to the office for a meeting and to the gym for a workout.

By the time I got home, I was tired from a long day and just collapsed and fell asleep. I am still trying to figure out why the chronological ordering is not working correctly and will work on it more tomorrow.

I can get on it with a fresh set of eyes tomorrow on Day Four.