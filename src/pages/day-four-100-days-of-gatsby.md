---
title: "Day Four, 100 Days Of Gatsby"
date: "2020-01-04"
---

Day Four was spent trying to get the chronological order fixed with this blog again, and again, to no avail. It appears that the query in GraphiQL just isn't responding for some reason and searches have turned up nothing for me online.

I'm tempted to take the whole query, copy it, delete everything, then paste the copied query and see if that works. Not sure if that's the things to do or not but I'd like this newset to oldest chronological order of blog posts to work soon.

EDIT: Chronological order descending is fixed and it looks like then a Markdown file is updated, it goes to the recent entry as well. Very intrigued why GraphiQL throws an error in the panel though.

```
query MyQuery {
  allFile {
    edges {
      node {
        id
        base
        accessTime
        size
        name
      }
    }
  }
  allMarkdownRemark(sort: {order: DESC, fields: [frontmatter___date]}) {
    edges {
      node {
        id
        excerpt(pruneLength: 250)
        fields {
          slug
        }
        frontmatter {
          title
          date
        }
      }
    }
  }
}
```

It works, not sure how though, but it works.