import React from "react"
import Layout from "../components/layout"
export default () => (
  <Layout>
    <h1>I'd love to talk! Email me at the address below</h1>
    <form name="contact" method="POST" netlify-honeypot="bot-field" data-netlify="true">
      <p class="hidden">
        <label>Don’t fill this out if you're human: <input name="bot-field" /></label>
      </p>
      <p>
        <label>Email: <input type="text" name="email" /></label>
      </p>
      <p>
        <label>Message: <textarea name="message"></textarea></label>
      </p>
      <p>
        <button type="submit">Send</button>
      </p>
    </form>
  </Layout>
)