---
title: "Day Forty Three, 100 Days Of Gatsby"
date: "2020-02-12"
---

## Quick Update

Well it has been awhile since I have touched this site but I finally got around to doing so.

I hadn't felt well in about a week, as I was knocked down by a rotten head cold. Before that, just very busy trying to get some projects wrapped up and get all my ducks in a row.

I was able to catch up with the #100DaysOfGatsby challenges without having to do much really. So I had that going for me. I had done the last 2 challenges ahead of time when I created the site and deployed it.

This last one, the latest that came out today was to turn the blog into a progressive web app. I don't have the means right now to do testing, but I'm pretty sure that I have that all set as well. I think you can get around here on a slow connection.

So that's where this stands right now, as for me, I am going to go back to getting some rest.

I do want to get reading time working on here at some point in time. I think that would be a great goal to achieve
