---
title: "Day Eight, 100 Days Of Gatsby"
date: "2020-01-08"
---

Well, I got home late tonight so I decided to give it a rest for the day. I didn't have any ideas other than working on a couple of the accessibility issues that were wrong with the blog.

Not sure what I am going to do next, I do have a couple of ideas, but tonight I decided to just kick back and not overthink it and take a break. Got a couple projects done, so that will free up more time to focus on this blog and some more Gatsby learning!