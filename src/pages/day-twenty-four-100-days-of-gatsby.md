---
title: "Day Twenty Four, 100 Days Of Gatsby"
date: "2020-01-24"
---

## Some Code

Boom! Got the code blocks working using PrismJS!

```javascript
import React from "react"
import { graphql } from "gatsby"
import Layout from "../components/layout"
import SEO from "../components/SEO"
export default ({ data }) => {
  const post = data.markdownRemark
  return (
    <Layout>
      <div>
        <h2>{post.frontmatter.title}</h2>
        <div dangerouslySetInnerHTML={{ __html: post.html }} />
        <SEO title="A GatsbyJS Blog" />
      </div>
    </Layout>
  )
}
```

```javascript
// In your gatsby-config.js
plugins: [
  {
    resolve: `gatsby-transformer-remark`,
    options: {
      plugins: [
        `gatsby-remark-prismjs`,
      ]
    }
  }
]
```
