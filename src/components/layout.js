import React from "react"
import { css } from "@emotion/core"
import { useStaticQuery, Link, graphql } from "gatsby"

import { rhythm } from "../utils/typography"
export default ({ children }) => {
  const data = useStaticQuery(
    graphql`
      query {
        site {
          siteMetadata {
            title
          }
        }
      }
    `
  )
  return (
    <div class="wrapper">
      <Link to={`/`}>
        <h1
          css={css`
            margin-bottom: ${rhythm(2)};
            display: inline-block;
          `}
        >
          {data.site.siteMetadata.title}
        </h1>
      </Link>
      <nav class="main">
        <Link
          to={`/about/`}
          css={css`
            float: right;
          `}
        >
          About
        </Link>
        <Link
          to={`/contact/`}
          css={css`
            float: right;
            margin: 0 1rem;
          `}
        >
          Contact
        </Link>
      </nav>
      {children}
    </div>
  )
}